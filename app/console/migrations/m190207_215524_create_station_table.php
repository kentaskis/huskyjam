<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%station}}`.
 */
class m190207_215524_create_station_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%station}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(50)->notNull()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%station}}');
    }
}
