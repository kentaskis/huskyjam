<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%carrier}}`.
 */
class m190207_215654_create_carrier_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%carrier}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(50)->notNull()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%carrier}}');
    }
}
