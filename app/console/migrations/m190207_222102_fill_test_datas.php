<?php

use yii\db\Migration;

/**
 * Class m190207_222102_fill_test_datas
 */
class m190207_222102_fill_test_datas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%station}}', ['name'], [
            ['Первая станция'],
            ['Вторая станция'],
            ['Третья станция'],
            ['Четвертая станция'],
            ['Пятая станция']
        ]);

        $this->batchInsert('{{%carrier}}', ['name'], [
            ['Перевозчик №1'],
            ['Перевозчик №2'],
            ['Перевозчик №3'],
            ['Перевозчик №4'],
            ['Перевозчик №5']
        ]);

        $this->batchInsert('{{%schedule}}', ['name'], [
            ['Ежедневно'],
            ['Пн.'],
            ['Вт.'],
            ['Ср.'],
            ['Чт.'],
            ['Пт.'],
            ['Сб.'],
            ['Вс.']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateTable('{{%carrier}}');
        $this->truncateTable('{{%station}}');
//        $this->truncateTable('{{%schedule}}');
        $this->execute('SET FOREIGN_KEY_CHECKS=1;');
    }


}
