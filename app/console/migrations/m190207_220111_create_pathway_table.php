<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pathway}}`.
 */
class m190207_220111_create_pathway_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%pathway}}', [
            'id' => $this->primaryKey()->unsigned(),
            'depart_time' => $this->time()->comment('Время отправления'),
            'arrival_time' => $this->time()->comment('Время прибытия'),
            'duration' => $this->time()->comment('Время в пути'),
            'depart_station_id' => $this->integer()->unsigned()->notNull()->comment('Станция отправления'),
            'arrival_station_id' => $this->integer()->unsigned()->notNull()->comment('Станция прибытия'),
            'price' => $this->money()->comment('Цена билета'),
            'carrier_id' => $this->integer()->unsigned()->comment('Перевозчик'),
        ],$tableOptions);

        $this->createIndex(
            'idx-pathway-arrival_station_id',
            '{{%pathway}}',
            'arrival_station_id'
        );
        $this->createIndex(
            'idx-pathway-depart_station_id',
            '{{%pathway}}',
            'depart_station_id'
        );
        $this->createIndex(
            'idx-pathway-carrier_id',
            '{{%pathway}}',
            'carrier_id'
        );

        $this->addForeignKey(
            'fk-pathway-arrival_station_id',
            '{{%pathway}}',
            'arrival_station_id',
            '{{%station}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-pathway-depart_station_id',
            '{{%pathway}}',
            'depart_station_id',
            '{{%station}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-pathway-carrier_id',
            '{{%pathway}}',
            'carrier_id',
            '{{%carrier}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-pathway-arrival_station_id',
            '{{%pathway}}'
        );
        $this->dropForeignKey(
            'fk-pathway-depart_station_id',
            '{{%pathway}}'
        );
        $this->dropForeignKey(
            'fk-pathway-carrier_id',
            '{{%pathway}}'
        );

        $this->dropIndex(
            'idx-pathway-arrival_station_id',
            '{{%pathway}}'
        );
        $this->dropIndex(
            'idx-pathway-depart_station_id',
            '{{%pathway}}'
        );
        $this->dropIndex(
            'idx-pathway-carrier_id',
            '{{%pathway}}'
        );

        $this->dropTable('{{%pathway}}');
    }
}
