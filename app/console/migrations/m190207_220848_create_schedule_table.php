<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%schedule}}`.
 */
class m190207_220848_create_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%schedule}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(10)
        ],$tableOptions);

        $this->createTable('{{%pathway_schedule}}', [
            'id' => $this->primaryKey()->unsigned(),
            'pathway_id' => $this->integer()->unsigned(),
            'schedule_id' => $this->integer()->unsigned()
        ],$tableOptions );

        $this->createIndex(
            'idx-pathway_schedule-pathway_id',
            '{{%pathway_schedule}}',
            'pathway_id'
        );
        $this->createIndex(
            'idx-pathway_schedule-schedule_id',
            '{{%pathway_schedule}}',
            'schedule_id'
        );

        $this->addForeignKey(
            'fk-pathway_schedule-schedule_id',
            '{{%pathway_schedule}}',
            'schedule_id',
            '{{%schedule}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-pathway_schedule-pathway_id',
            '{{%pathway_schedule}}',
            'pathway_id',
            '{{%pathway}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-pathway_schedule-schedule_id',
            '{{%pathway_schedule}}'
        );
        $this->dropForeignKey(
            'fk-pathway_schedule-pathway_id',
            '{{%pathway_schedule}}'
        );
        $this->dropIndex(
            'idx-pathway_schedule-pathway_id',
            '{{%pathway_schedule}}'
        );
        $this->dropIndex(
            'idx-pathway_schedule-schedule_id',
            '{{%pathway_schedule}}'
        );

        $this->dropTable('{{%pathway_schedule}}');
        $this->dropTable('{{%schedule}}');
    }
}
