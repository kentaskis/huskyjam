<?php

use yii\db\Migration;

/**
 * Class m190207_214451_create_first_admin_user_for_test
 */
class m190207_214451_create_first_admin_user_for_test extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = new \frontend\models\SignupForm();
        $model->username = 'admin';
        $model->email = "kentaskis.l@gmail.com";
        $model->password = 'admin4site';
        $model->signup();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \common\models\User::findOne(1)->delete();

    }

}
