<?php
/**
 * Created by LeLiK.
 * Date: 11.02.2019
 * Time: 20:00
 * Lavrov Alexey <kentaskis.l@gmail.com>
 */

namespace app\resources;


use common\models\Pathway;



/**
 * @apiDefine ModelPathwayAttributes
 *
 * @apiSuccess {int} id Pathway Id
 * @apiSuccess {string} depart_time Время отправления
 * @apiSuccess {string} arrival_time Время прибытия
 * @apiSuccess {string} duration Время в пути
 * @apiSuccess {int} depart_station_id Станция отправления
 * @apiSuccess {int} arrival_station_id Станция прибытия
 * @apiSuccess {string} price Цена билета
 * @apiSuccess {int} carrier_id Перевозчик
 */

/**
 * @apiDefine PathwayModifyRequest
 *
 * @apiParamExample {json} Pathway-Request:
 *
    {
        "depart_time": "12:20",
        "arrival_time": "20:40",
        "duration": "8:20",
        "price": "78.22",
        "schedule_ids": [1, 2],
        "arrival_station_id": 2,
        "carrier_id": 3,
        "depart_station_id": 4
    }
 *
 */




/**
 * Class PathwayResource
 * @package frontend\resources
 */

class PathwayResource extends Pathway
{
    public function fields()
    {
        $fields = parent::fields();

        $fields[] = 'arrivalStation';
        $fields[] = 'departStation';
        $fields[] = 'carrier';
        $fields[] = 'schedulesName';

        unset($fields['depart_station_id']);
        unset($fields['arrival_station_id']);
        unset($fields['carrier_id']);

        return $fields;
    }
}