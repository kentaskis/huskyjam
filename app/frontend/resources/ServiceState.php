<?php

namespace app\resources;

use yii\base\Model;
use yii\helpers\Url;
use yii\web\Linkable;

class ServiceState extends Model implements Linkable
{
    public $id;
    public $name;
    public $time;
    public $datetime;
    public $timezone;
    public $env;
    public $debug;
    public $php;
    public $database;

    public function init()
    {
        parent::init();

        $this->id       = \Yii::$app->id;
        $this->name     = \Yii::$app->name;
        $this->time     = time();
        $this->timezone = \Yii::$app->timeZone;
        $this->datetime = date('Y-m-d H:i:s');
        $this->debug    = YII_DEBUG;
        $this->env      = YII_ENV;
        $this->php      = [
            'post_max_size' => ini_get('post_max_size'),
            'version'       => phpversion(),
        ];

    }

    public function getLinks()
    {
        return [
            'docs' => Url::to(['/docs'],true),
        ];
    }

}