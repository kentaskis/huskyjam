<?php


namespace frontend\controllers;


use app\resources\PathwayResource;

use yii\rest\ActiveController;

class PathwayController extends ActiveController
{
    public $modelClass = PathwayResource::class;

}

/**
 * @api {GET} /pathway 01. Listing of pathways
 * @apiGroup Pathway
 * @apiName getListingPathways
 * @apiUse ModelPathwayAttributes
 * @apiUse PathwayModifyRequest
 * @apiVersion 0.1.0
 */

/**
 * @api {POST} /pathway 02. Create pathway
 * @apiGroup Pathway
 * @apiName postCreatePathway
 * @apiVersion 0.1.0
 * @apiDescription
 *   Creates a new activity.
 */

/**
 * @api {GET} /pathway/:id 03. Show pathway by id
 * @apiGroup Pathway
 * @apiName getShowPathway
 * @apiVersion 0.1.0
 */

/**
 * @api {PUT} /pathway/:id 04. Modify pathway by id
 * @apiGroup Pathway
 * @apiName putModifyPathway
 * @apiUse PathwayModifyRequest
 * @apiVersion 0.1.0
 */

/**
 * @api {DELETE} /pathway/:id 05. Delete pathway by id
 * @apiGroup Pathway
 * @apiName deletePathway
 * @apiVersion 0.1.0
 */


