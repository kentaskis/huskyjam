<?php
namespace frontend\controllers;

use app\resources\ServiceState;
use yii\web\Controller;


/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionIndex(): ServiceState
    {
        return new ServiceState();
    }

}
