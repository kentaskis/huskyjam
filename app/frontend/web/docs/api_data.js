define({ "api": [
  {
    "type": "DELETE",
    "url": "/pathway/:id",
    "title": "05. Delete pathway by id",
    "group": "Pathway",
    "name": "deletePathway",
    "version": "0.1.0",
    "filename": "frontend/controllers/PathwayController.php",
    "groupTitle": "Pathway"
  },
  {
    "type": "GET",
    "url": "/pathway",
    "title": "01. Listing of pathways",
    "group": "Pathway",
    "name": "getListingPathways",
    "version": "0.1.0",
    "filename": "frontend/controllers/PathwayController.php",
    "groupTitle": "Pathway",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>Pathway Id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "depart_time",
            "description": "<p>Время отправления</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "arrival_time",
            "description": "<p>Время прибытия</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "duration",
            "description": "<p>Время в пути</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "depart_station_id",
            "description": "<p>Станция отправления</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "arrival_station_id",
            "description": "<p>Станция прибытия</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "price",
            "description": "<p>Цена билета</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "carrier_id",
            "description": "<p>Перевозчик</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Pathway-Request:",
          "content": "\n{\n    \"depart_time\": \"12:20\",\n    \"arrival_time\": \"20:40\",\n    \"duration\": \"8:20\",\n    \"price\": \"78.22\",\n    \"schedule_ids\": [1, 2],\n    \"arrival_station_id\": 2,\n    \"carrier_id\": 3,\n    \"depart_station_id\": 4\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "GET",
    "url": "/pathway/:id",
    "title": "03. Show pathway by id",
    "group": "Pathway",
    "name": "getShowPathway",
    "version": "0.1.0",
    "filename": "frontend/controllers/PathwayController.php",
    "groupTitle": "Pathway"
  },
  {
    "type": "POST",
    "url": "/pathway",
    "title": "02. Create pathway",
    "group": "Pathway",
    "name": "postCreatePathway",
    "version": "0.1.0",
    "description": "<p>Creates a new activity.</p>",
    "filename": "frontend/controllers/PathwayController.php",
    "groupTitle": "Pathway"
  },
  {
    "type": "PUT",
    "url": "/pathway/:id",
    "title": "04. Modify pathway by id",
    "group": "Pathway",
    "name": "putModifyPathway",
    "version": "0.1.0",
    "filename": "frontend/controllers/PathwayController.php",
    "groupTitle": "Pathway",
    "parameter": {
      "examples": [
        {
          "title": "Pathway-Request:",
          "content": "\n{\n    \"depart_time\": \"12:20\",\n    \"arrival_time\": \"20:40\",\n    \"duration\": \"8:20\",\n    \"price\": \"78.22\",\n    \"schedule_ids\": [1, 2],\n    \"arrival_station_id\": 2,\n    \"carrier_id\": 3,\n    \"depart_station_id\": 4\n}",
          "type": "json"
        }
      ]
    }
  }
] });
