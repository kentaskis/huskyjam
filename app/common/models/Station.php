<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "station".
 *
 * @property int $id
 * @property string $name
 *
 * @property Pathway[] $pathways
 * @property Pathway[] $pathways0
 */
class Station extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName() : string
    {
        return 'station';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() : array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPathways() : ActiveQuery
    {
        return $this->hasMany(Pathway::class, ['arrival_station_id' => 'id']);
    }

}
