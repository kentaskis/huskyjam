<?php

namespace common\models\searches;

use common\models\Pathway;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PathwaySearch represents the model behind the search form of `app\models\Pathway`.
 */
class PathwaySearch extends Pathway
{
    /**
     * {@inheritdoc}
     */
    public function rules() : array
    {
        return [
            [['id', 'depart_station_id', 'arrival_station_id', 'carrier_id'], 'integer'],
            [['depart_time', 'arrival_time', 'duration'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() : array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) : ActiveDataProvider
    {
        $query = Pathway::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'depart_time' => $this->depart_time,
            'arrival_time' => $this->arrival_time,
            'duration' => $this->duration,
            'depart_station_id' => $this->depart_station_id,
            'arrival_station_id' => $this->arrival_station_id,
            'price' => $this->price,
            'carrier_id' => $this->carrier_id,
        ]);

        return $dataProvider;
    }
}
