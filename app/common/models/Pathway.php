<?php

namespace common\models;

use voskobovich\behaviors\ManyToManyBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pathway".
 *
 * @property int $id
 * @property string $depart_time Время отправления
 * @property string $arrival_time Время прибытия
 * @property string $duration Время в пути
 * @property int $depart_station_id Станция отправления
 * @property int $arrival_station_id Станция прибытия
 * @property string $price Цена билета
 * @property int $carrier_id Перевозчик
  *
 * @property Station $arrivalStation
 * @property Carrier $carrier
 * @property Station $departStation
 * @property Schedule[] $schedules
 */
class Pathway extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName() : string
    {
        return 'pathway';
    }

    public function behaviors()
    {
        return [
            [
                'class' => ManyToManyBehavior::class,
                'relations' => [
                    'schedule_ids' => 'schedules',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules() : array
    {
        return [
//            [['depart_time', 'arrival_time', 'duration'], 'safe'],
            [['depart_station_id', 'arrival_station_id'], 'required'],
            [['depart_station_id', 'arrival_station_id', 'carrier_id'], 'integer'],
            [['price'], 'number'],
            [['depart_time', 'arrival_time', 'duration'], 'date','format' => 'H:m'],
            [['arrival_station_id'], 'exist', 'skipOnError' => true, 'targetClass' => Station::class, 'targetAttribute' => ['arrival_station_id' => 'id']],
            [['carrier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Carrier::class, 'targetAttribute' => ['carrier_id' => 'id']],
            [['depart_station_id'], 'exist', 'skipOnError' => true, 'targetClass' => Station::class, 'targetAttribute' => ['depart_station_id' => 'id']],
            [['schedule_ids'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'depart_time' => 'Время отправления',
            'arrival_time' => 'Время прибытия',
            'duration' => 'Время в пути',
            'departStation' => 'Станция отправления',
            'arrivalStation' => 'Станция прибытия',
            'price' => 'Цена билета',
            'carrier' => 'Перевозчик',
            'schedule_ids' => 'График движения',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getArrivalStation() : ActiveQuery
    {
        return $this->hasOne(Station::class, ['id' => 'arrival_station_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCarrier() : ActiveQuery
    {
        return $this->hasOne(Carrier::class, ['id' => 'carrier_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDepartStation() : ActiveQuery
    {
        return $this->hasOne(Station::class, ['id' => 'depart_station_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSchedules() : ActiveQuery
        {
        return $this->hasMany(Schedule::class,['id'=>'schedule_id'])
            ->viaTable('{{%pathway_schedule}}',['pathway_id' => 'id']);
    }

    public function getSchedulesName() : string
        {
        return implode(', ', ArrayHelper::getColumn($this->schedules,'name'));
    }

}
