<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "carrier".
 *
 * @property int $id
 * @property string $name
 *
 * @property Pathway[] $pathways
 */
class Carrier extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName() : string
    {
        return 'carrier';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() : array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPathways() : ActiveQuery
    {
        return $this->hasMany(Pathway::class, ['carrier_id' => 'id']);
    }
}
