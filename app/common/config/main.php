<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'formatter' => [
            'dateFormat' => 'php:d-m-Y',
            'datetimeFormat'=>'php:d-M-Y H:i',
            'timeFormat' => 'php:H:i'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
