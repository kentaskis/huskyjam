<?php

namespace backend\controllers;

use common\models\Carrier;
use common\models\Schedule;
use common\models\Station;
use Yii;
use common\models\Pathway;
use common\models\searches\PathwaySearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PathwayController implements the CRUD actions for Pathway model.
 */
class PathwayController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pathway models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PathwaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Pathway model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pathway();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'stations' => ArrayHelper::map(Station::find()->all(),'id','name'),
            'carriers' => ArrayHelper::map(Carrier::find()->asArray()->all(),'id','name'),
            'schedules' => ArrayHelper::map(Schedule::find()->asArray()->all(),'id','name'),
        ]);
    }

    /**
     * Updates an existing Pathway model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'stations' => ArrayHelper::map(Station::find()->all(),'id','name'),
            'carriers' => ArrayHelper::map(Carrier::find()->asArray()->all(),'id','name'),
            'schedules' => ArrayHelper::map(Schedule::find()->asArray()->all(),'id','name'),

        ]);
    }


    /**
     * Finds the Pathway model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pathway the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pathway::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
