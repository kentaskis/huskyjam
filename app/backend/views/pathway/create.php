<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pathway */

$this->title = 'Create Pathway';
$this->params['breadcrumbs'][] = ['label' => 'Pathways', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pathway-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form',
        compact('model','stations','carriers','schedules')
    ) ?>

</div>
