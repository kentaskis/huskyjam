<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pathway */

$this->title = 'Update Pathway: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pathways', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pathway-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model','stations','carriers','schedules')) ?>

</div>
