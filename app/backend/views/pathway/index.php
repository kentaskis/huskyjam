<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\searches\PathwaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pathways';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pathway-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pathway', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'depart_time',
                'value' => function ($model, $key, $index, $widget) {
                    return date("H:i", strtotime($model->depart_time));}
            ],
            [
                'attribute' => 'arrival_time',
                'value' => function ($model, $key, $index, $widget) {
                    return date("H:i", strtotime($model->arrival_time));}
            ],
            [
                'attribute' => 'duration',
                'value' => function ($model, $key, $index, $widget) {
                    return date("H:i", strtotime($model->duration));}
            ],
            [
                'attribute' => 'arrivalStation',
                'value' => 'arrivalStation.name'
            ],
            [
                'attribute' => 'departStation',
                'value' => 'departStation.name'
            ],
            'price',
            [
                'attribute' => 'carrier',
                'value' => 'carrier.name'
            ],
            [
                'attribute' => 'schedule_ids',
                'value' => function ($model, $key, $index, $widget) {
                            return implode(', ',\yii\helpers\ArrayHelper::getColumn($model->schedules,'name'));}
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}'
                ],
        ],
    ]); ?>
</div>
