<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Pathway */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pathway-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'depart_time')->textInput()->textInput(['placeholder' => "Например: 8:00"]) ?>

    <?= $form->field($model, 'arrival_time')->textInput()->textInput(['placeholder' => "Например: 16:20"]) ?>

    <?= $form->field($model, 'duration')->textInput(['placeholder' => "Например: 8:20"]) ?>

    <?= $form->field($model, 'depart_station_id')->dropDownList($stations,['prompt' => 'Выберите станцию...']) ?>

    <?= $form->field($model, 'arrival_station_id')->dropDownList($stations,['prompt' => 'Выберите станцию...']) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carrier_id')->dropDownList($carriers,['prompt' => 'Выберите перевозчика...']) ?>

    <?= $form->field($model, 'schedule_ids')->dropDownList($schedules,['multiple' => true])->label('График движения ( Ctrl - для выбора нескольких опций)') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
